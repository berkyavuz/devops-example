# devops-example

Automated simple application.

## Index
* [Links](#links)
* [About Application](#about-application)
* [Dockerfile](#dockerfile)
* [Branch](#branch)
* [Cluster Setup](#cluster-setup)
* [Pipeline](#pipeline)
* [Ansible](#ansible)
* [Kubernetes Resources](#kubernetes-resources)
* [Example Flow (Must See)](#example-flow)



## Links

- [Dockerhub](https://hub.docker.com/repository/docker/berkyavuz/devops-example/tags?page=1&ordering=last_updated)
- [https://gitlab.com/berkyavuz/devops-example](https://gitlab.com/berkyavuz/devops-example)
- [Application](http://159.89.27.124:11130/)


### About Application

This application is `nodejs` application that depended to `expressJs`. It reads the `PORT` value and `COMPANY_NAME` from the environment to gain flexibility.
See `config.js` under the `app/src` to examine.

## Dockerfile

Dockerfile contains the instructions below;

- All the files that should not be in the container prevented from the COPY command by .dockerignore file.

```Dockerfile
FROM node:16.6.0-stretch-slim
USER node
WORKDIR /home/node
RUN mkdir app
COPY . app
WORKDIR /home/node/app
RUN npm install
ENTRYPOINT npm run start
```

## Branch

- `master` is release branch and not allowed to push directly (Protected Branch). The code have to merged from `dev -> master`.
- Using `CI_PIPELINE_IID` predefined variable for docker image tagging.

## Cluster Setup

1. I rent a droplet from digitalocean (Because it is cheap :) )
2. I setup single node cluster with kubeadm. I followed a guide that written by me. Please examine! [https://github.com/berkyvz/k8s/blob/master/kubeadm/full-installation.md](https://github.com/berkyvz/k8s/blob/master/kubeadm/full-installation.md)
    - Containing metric-server, weavenet for pod network.

## Pipeline

- Please examine the [last part](#example-flow) to understand more clearly

- Pipeline is basically takes the code and build a new image and pushing it to my [dockerhub](https://hub.docker.com/repository/docker/berkyavuz/devops-example/tags?page=1&ordering=last_updated).
- Then, runs the ansible playbook on the my server that is provided by digitalocean to deploy newly created image. (This part could be done better by using helm charts.)

- variables:
   - `IMAGE_NAME_VERSION`: image version according to pipeline.
   - `IMAGE_NAME_WITH_VERSION`: image name for tagging latest image with spesific version number.
   - `IMAGE_NAME_WITH_LATEST`: image name for tagging latest image.
   - `SERVER_IP`: My server IP
- masked variables from gitlab CI/CD Settings.
    - `DOCKERHUB_PASSWORD` : masked password in order to use when logging in.
    - `SSH_PRIVATE_KEY`: private key of the server.

```yaml
stages:
  - build
  - deploy

variables:
  IMAGE_NAME_VERSION: 1.$CI_PIPELINE_IID
  IMAGE_NAME_WITH_VERSION: berkyavuz/$CI_PROJECT_NAME:1.$CI_PIPELINE_IID
  IMAGE_NAME_WITH_LATEST: berkyavuz/$CI_PROJECT_NAME:latest
  SERVER_IP: 159.89.27.124


docker-build:
  only:
    - master
  image: docker:latest
  stage: build
  services:
    - docker:dind
  before_script:
    - echo "$DOCKERHUB_PASSWORD" | docker login --username berkyavuz --password-stdin
  script:
    - cd ./app && docker build -t "$IMAGE_NAME_WITH_VERSION" .
    - docker tag "$IMAGE_NAME_WITH_VERSION" "$IMAGE_NAME_WITH_LATEST"
    - docker push "$IMAGE_NAME_WITH_VERSION"
    - docker push "$IMAGE_NAME_WITH_LATEST"

deploy-digital-ocean:
  only:
    - master
  stage: deploy
  image: ubuntu
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client git -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan $SERVER_IP >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - apt update -y
    - apt install ansible -y
    - echo "[servers]" >> /etc/ansible/hosts && echo "digitalocean ansible_host=$SERVER_IP" >> /etc/ansible/hosts && echo "[all:vars]" >> /etc/ansible/hosts && echo "ansible_python_interpreter=/usr/bin/python3" >> /etc/ansible/hosts
  script:
    - ls -lrt
    - ansible digitalocean -m ping -u root
    # giving latest image tag from the ansbile variables.
    - ansible-playbook  ./infrastructure/ansible/deploy-new-app-version.yaml -e "image_tag=$IMAGE_NAME_VERSION" 
```

## Ansible
__Ansible configuration file that is executed by pipeline for each commit on master__

```yaml
- name: Deploying Application
  hosts: servers
  remote_user: root
  become: true
  vars:
    - image: berkyavuz/devops-example:{{ image_tag }}
    - k8s_resources:

  tasks:
    - debug: var=image_tag
      when: image_tag is defined

    # CLONE OR PULL PROJECT
    - name: Clone Project A git repo
      git:
        version: master
        repo: 'https://gitlab.com/berkyavuz/devops-example.git'
        dest: /root/devops-example

    # APPLY MEW RESOURCES AND UPDATE THE DEPLOYMENT IMAGE WITH THE LATEST VERSION
    - name: Apply kubernetes resources in order to any change.
      shell: |
        kubectl apply -f .
        kubectl set image deployment/devops-example-deployment devops-example-container={{ image }} --record -n devops-example
        exit 0
      args:
        chdir: /root/devops-example/infrastructure/k8s

    # RUNNING SH SCRIPT ON SERVER TO GET OUTPUT OF THE "kubectl get all --all-namespaces"
    - name: Print k8s resources
      shell: ./print-k8s-resources.sh
      register: k8s_resources
      args:
        chdir: /root/devops-example/infrastructure/ansible

    # PRINTING K8s RESOURCES
    - debug: 
        var: k8s_resources.stdout_lines
```

## Kubernetes resources

__Deployment__
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: devops-example-deployment
  namespace: devops-example
  labels:
    app: devops-example
    version: v1
spec:
  replicas: 1
  strategy: 
    rollingUpdate:
      maxSurge: 50%
      maxUnavailable: 25%
    type: RollingUpdate
  selector:
    matchLabels:
      app: devops-example
      version: v1
  revisionHistoryLimit: 1
  template:
    metadata:
      labels:
        app: devops-example
        version: v1
    spec: 
      containers:
        - name: devops-example-container
          image: berkyavuz/devops-example:latest
          envFrom:
          - configMapRef:
             name: devops-example-config
          readinessProbe: 
            httpGet:
              path: /status
              port: http
            timeoutSeconds: 10
            periodSeconds: 10
            initialDelaySeconds: 10
          livenessProbe: 
            httpGet:
              path: /status
              port: http
            timeoutSeconds: 10
            periodSeconds: 10
            initialDelaySeconds: 5 
          imagePullPolicy: Always
          ports:
          - name: http
            containerPort: 11130    
          resources:
            limits:
              cpu: 500m
              memory: 512Mi
            requests:
              cpu: 250m
              memory: 256Mi
      restartPolicy: Always
```
__Service__:
```yaml
apiVersion: v1
kind: Service
metadata:
  name: devops-example-service
  namespace: devops-example
spec:
  selector:
    app: devops-example
    version: v1
  ports:
    - protocol: TCP
      port: 11130
      targetPort: 11130
  externalIPs:
    - 159.89.27.124
```
__Configmap__: to read env variables that used by application.
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: devops-example-config
  namespace: devops-example
  labels:
    app: devops-example
    version: v1
data:
  COMPANY_NAME: "Hepsiburada"
  PORT: "11130"
```
__Hpa__: Horizontal scaling based on memory and cpu usage. (Could use prometheus to make it based on reqest)
```yaml
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: devops-example-horizontal-scaler
  namespace: devops-example
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: devops-example
  minReplicas: 1
  maxReplicas: 4
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: 30
  - type: Resource
    resource:
      name: memory
      target:
        type: Utilization
        averageUtilization: 30

```

## Example Flow

1. New code source pushed to dev `branch`.
2. Merget to `master` with `merge request`.
3. Pipeline triggered automatically. 
    - Create new image based on pipeline id which is incremented by 1 (__Should use semantic versioning__)
    - Create new image with latest tag also and push them to dockerhub.
    ```yaml
    docker-build:
        only:
            - master
        image: docker:latest
        stage: build
        services:
            - docker:dind
        before_script:
            - echo "$DOCKERHUB_PASSWORD" | docker login --username berkyavuz --password-stdin
        script:
            - cd ./app && docker build -t "$IMAGE_NAME_WITH_VERSION" .
            - docker tag "$IMAGE_NAME_WITH_VERSION" "$IMAGE_NAME_WITH_LATEST"
            - docker push "$IMAGE_NAME_WITH_VERSION"
            - docker push "$IMAGE_NAME_WITH_LATEST"
    ```
    ![dockerhub new images](./img/dockerhub.jpg)
    - To run ansible the job below works
        ```yaml
        deploy-digital-ocean:
            only:
                - master
            stage: deploy
            image: ubuntu
            before_script:
                - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client git -y )'
                - eval $(ssh-agent -s)
                - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
                - mkdir -p ~/.ssh
                - chmod 700 ~/.ssh
                - ssh-keyscan $SERVER_IP >> ~/.ssh/known_hosts
                - chmod 644 ~/.ssh/known_hosts
                - apt update -y
                - apt install ansible -y
                - echo "[servers]" >> /etc/ansible/hosts && echo "digitalocean ansible_host=$SERVER_IP" >> /etc/ansible/hosts && echo "[all:vars]" >> /etc/ansible/hosts && echo "ansible_python_interpreter=/usr/bin/python3" >> /etc/ansible/hosts
            script:
                - ls -lrt
                - ansible digitalocean -m ping -u root
                # giving latest image tag from the ansbile variables.
                - ansible-playbook  ./infrastructure/ansible/deploy-new-app-version.yaml -e "image_tag=$IMAGE_NAME_VERSION" 
        ```
    - Run ansible playbook
        - Check `image tag` which is passed from the pipeline to give new image tag information to ansible.
        ```yaml
            vars:
                - image: berkyavuz/devops-example:{{ image_tag }}
                - k8s_resources:

            tasks:
                - debug: var=image_tag
                  when: image_tag is defined
        ```
        - Apply kubernetes resources and set deployment's container image with the image that we created just before to trigger rolling update. (Should use helm chart)
        ![deployment](./img/image_pull_deployment.jpg)
4.  Print all resources in the kubernetes with bash script on the pipeline.. `/infrastructure/ansible/print-k8s-resources.sh`
        ![k8s-reources](./img/print-k8s.jpg)

5. You can look for the [pipeline logs](https://gitlab.com/berkyavuz/devops-example/-/pipelines/346182393)


## Author: Berk Yavuz



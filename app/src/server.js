const express = require('express');
const config = require('./config').config;
const app = express();


app.get('/', (req, res) => {
  res.status(200);
  res.send(`Hello ${config.companyName} from Berk Yavuz`);
});

app.get('/status', (req, res) => {
  res.status(200);
  res.send({"serverStatus":"UP"});
});

app.listen(config.port, () => {
  console.log(`App listening at ${config.port}`)
});
// for development purposes.
require('dotenv').config();

const config = {
    port: process.env.PORT || 3000,
    companyName: process.env.COMPANY_NAME || 'DreamSoftwareCompany'
};

exports.config = config;